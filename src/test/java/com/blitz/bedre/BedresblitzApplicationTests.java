package com.blitz.bedre;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DronezBlitzApp.class)
public class BedresblitzApplicationTests {

	@Test
	public void contextLoads() {
	}

}
