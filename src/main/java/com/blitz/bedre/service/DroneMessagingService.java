package com.blitz.bedre.service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.blitz.bedre.csv.CsvFileImporter;
import com.blitz.bedre.drone.MessageListener;
import com.blitz.bedre.model.location.DronePosition;
import com.blitz.bedre.model.message.DroneShutDownMessage;
import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.model.message.MessageHandler;

@Component
public class DroneMessagingService implements MessagingService {

	private static final Logger logger = LoggerFactory.getLogger(DroneMessagingService.class);

	private static final int DRONE_MAX_MESSAGES_CAPACITY = 10;
	public static final String DRONE_6043_ID = "6043";
	public static final String DRONE_5937_ID = "5937";
	private static final LocalTime TIME_FOR_DRONES_SHUTDOWN = LocalTime.of(8, 10);

	private Map<String, Integer> droneMessageCapacity = new HashMap<String, Integer>();

	private ExecutorService executor = Executors.newSingleThreadExecutor();

	private Map<String, MessageListener> droneListeners = new HashMap<String, MessageListener>();
	
	private List<MessageHandler> trafficReportListeners = new ArrayList<MessageHandler>(); 
	
	private CsvFileImporter csvFileImporter;
	
	private BlockingQueue<Message> inboundMessagesQueue = new LinkedBlockingQueue<Message>();
	
	private volatile boolean running = false;
	
	@Autowired
	public DroneMessagingService(CsvFileImporter csvFileImporter){
		this.csvFileImporter = csvFileImporter;
	}

	@PostConstruct
	private void init() {
		droneMessageCapacity.put(DRONE_6043_ID, 0);
		droneMessageCapacity.put(DRONE_5937_ID, 0);
		csvFileImporter.init();
		executor.execute(new ExecutorTask());
		running = true;
	}
	
	@Override
	public void handleMessage (Message message){
		inboundMessagesQueue.add(message);
	}
	
	@PreDestroy
	public void stop() {
		running = false;
		executor.shutdown();
	}

	
	private void sendDronePositionsToDrone(Message message) {
		String droneId = message.getSource();

		Integer currentDroneMessagesCount = droneMessageCapacity.get(droneId);

		Integer reportedAvailableMessagesCapacity = Integer.valueOf(message.getPayload());

		if (currentDroneMessagesCount != null) {
			// if (currentDroneMessagesCount < DRONE_MAX_MESSAGES_CAPACITY) {
			if (0 < reportedAvailableMessagesCapacity
					&& reportedAvailableMessagesCapacity < DRONE_MAX_MESSAGES_CAPACITY) {
				
				currentDroneMessagesCount = reportedAvailableMessagesCapacity;
				
				for (int i = 0; i < reportedAvailableMessagesCapacity; i++) {
					DronePosition position = getNextPositionFromQueue(message);
					if (position != null && !isTimeForShutdownSignal(position)) {
						boolean hasProcessedMessage = droneListeners.get(droneId).processPositionMessage(position);
						currentDroneMessagesCount++;
						if (hasProcessedMessage) {
							logger.info("Drone " + droneId + " processed position message: " + position.toString());
						}
					} else {
						Message droneShutDownMessage = new DroneShutDownMessage("Dispatch", droneId);
						droneListeners.get(droneId).processShutdownMessage(droneShutDownMessage);
						break;
					}
				}
			} else {
				logger.error("Not a valid reported number of available positions:" + reportedAvailableMessagesCapacity);
			}
		} else {
			logger.error("Nosaj drone ID: " + droneId);
		}
	}

	private boolean isTimeForShutdownSignal(DronePosition dronePosition) {
		LocalTime time = dronePosition.getTime();
		return TIME_FOR_DRONES_SHUTDOWN.isAfter(time);
	}

	//this is where traffic report messages are being sent to be persisted to the traffic dispatcher
	@Override
	public void processMessageFromDrone(Message message) {
		//notify registered listeners - i.e. the TrafficManager
		for(MessageHandler trafficManager : trafficReportListeners){
			if (trafficManager.canHandle(message)){
				trafficManager.execute(message);
			}
		}
	}

	@Override
	public void registerConsumers(MessageListener messageConsumer) {
		droneListeners.put(messageConsumer.getConsumerKey(), messageConsumer);
	}
	
	public void registerTrafficManagerListener(MessageHandler trafficManager){
		trafficReportListeners.add(trafficManager);
	}

	@Override
	public void onMessage() {
		// TODO Auto-generated method stub

	}

	private DronePosition getNextPositionFromQueue(Message message) {		
		return csvFileImporter.getNextPositionFromQueue(message);
	}

	private class ExecutorTask implements Runnable {
		
		@Override
		public void run() {
			while (running) {
				try {
					Message message = inboundMessagesQueue.poll(5, TimeUnit.SECONDS);
					
					if (message != null) {
						switch (message.getType()) {
						case TRAFFIC_REPORT:
							processMessageFromDrone(message);
							break;
						case REQUEST_LOCATION:
							sendDronePositionsToDrone(message);
							break;
						default:
							logger.error("Unknow Message Type received: " + message.getType().name());
						}
					}
				} catch (InterruptedException iex) {
					logger.error("Interrupted while polling for messages ..." + inboundMessagesQueue.size());
				}
					
					
			}
			
		}

	}
}
