package com.blitz.bedre.service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.blitz.bedre.model.location.DronePosition;
import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.model.message.MessageHandler;
import com.blitz.bedre.model.message.MsgType;

@Component
public class AsyncTrafficManager implements MessageHandler{

	private static final Logger logger = LoggerFactory.getLogger(AsyncTrafficManager.class);

	private ThreadPoolTaskExecutor taskExecutor;

	private BlockingQueue<DronePosition> drone6043PositionsQueue = new LinkedBlockingQueue<DronePosition>();
	private BlockingQueue<DronePosition> drone5937PositionsQueue = new LinkedBlockingQueue<DronePosition>();

	private Map<MsgType, MessageHandler> messageHandlers = new ConcurrentHashMap<MsgType, MessageHandler>();

	private BlockingQueue<Message> messagesQueue = new LinkedBlockingQueue<Message>();
	
	@Autowired
	private DroneMessagingService droneMessagingService;

	private volatile boolean running = false;

	@PostConstruct
	private void init() {

		taskExecutor = new ThreadPoolTaskExecutor();

		taskExecutor.setCorePoolSize(2);
		taskExecutor.setMaxPoolSize(4);
		taskExecutor.setAwaitTerminationSeconds(10);
		taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
		taskExecutor.setBeanName(this.getClass().getSimpleName()+ "ThreadPoolExecutor");
		
		droneMessagingService.registerTrafficManagerListener(this);
		
		running = true;
	}

	@PreDestroy
	public void stop() {
		running = false;
		taskExecutor.shutdown();
	}

	public void registerEventHandler(MsgType messageType, MessageHandler messageHandler) {
		messageHandlers.put(messageType, messageHandler);
	}

	public void handleMessage(Message message) {
		if (!messagesQueue.offer(message)) {
			logger.error("Cannot add message into AsysncTrafficManagers messages queue: " + messagesQueue.size());
		}
	}

	public void executeEvent(Message message) {

		while (running) {
			try {
				message = messagesQueue.poll(5, TimeUnit.SECONDS);

				if (message != null) {
					switch (message.getType()) {
					case TRAFFIC_REPORT:
						this.executeInExecutor(message);
						break;
					case REQUEST_LOCATION:
						droneMessagingService.handleMessage(message);
						break;
					case DRONE_MOVE:
						getNextPositionFromQueue(message);
						break;
					default:
						logger.error("Unknow Message Type received: " + message.getType().name());
					}
				}
			} catch (InterruptedException iex) {
				logger.error("Interrupted while polling for messages ..." + messagesQueue.size());
			}

		}

	}

	private DronePosition getNextPositionFromQueue(Message message) {
		if ("6043".equals(message.getSource()))
			return drone6043PositionsQueue.poll();
		else if ("5937".equals(message.getSource()))
			return drone5937PositionsQueue.poll();
		else
			logger.error("There is no Drone registered with ID: " + message.getSource());
		return null;
	}

	private void executeInExecutor(Message message) {

		this.taskExecutor.execute(new Runnable() {

			@Override
			public void run() {
				try {
					AsyncTrafficManager.this.messageHandlers.get(message.getType()).execute(message);
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}
			}
		});
	}

	@Override
	public MsgType getSupportedMessageType() {
		return MsgType.TRAFFIC_REPORT;
	}

	@Override
	public boolean canHandle(Message message) {
		return getSupportedMessageType().equals(message.getType());
	}

	@Override
	public void execute(Message message) {
		this.messagesQueue.offer(message);
	}

}
