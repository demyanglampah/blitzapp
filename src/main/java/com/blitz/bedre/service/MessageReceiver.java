package com.blitz.bedre.service;

public interface MessageReceiver {
	
	void registerReceiver();
	
	void handleMessage();

}
