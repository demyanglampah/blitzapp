package com.blitz.bedre.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.blitz.bedre.model.message.Message;

public class MessagesBroker {

	private BlockingQueue<Message> dronePositionsQueue = new LinkedBlockingQueue<Message>();
	private boolean stillExpectingMessages = true;
	
	public void put(Message data) throws InterruptedException
    {
        this.dronePositionsQueue.put(data);
    }
 
    public Message get() throws InterruptedException
    {
        return this.dronePositionsQueue.poll(1, TimeUnit.SECONDS);
    }
    
    public boolean isStillExpectingMessages(){
    	return stillExpectingMessages;
    }
    
    public void setStillExpectingMessages(boolean expectingMessages){
    	this.stillExpectingMessages = expectingMessages;
    }
}
