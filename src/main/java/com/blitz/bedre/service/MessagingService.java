package com.blitz.bedre.service;

import com.blitz.bedre.drone.MessageListener;
import com.blitz.bedre.model.message.Message;

public interface MessagingService {
	
	void handleMessage(Message outoundMessage);
	
	void processMessageFromDrone(Message inboundMessage);
	
	void registerConsumers(MessageListener messageConsumer);
	
	void onMessage();
}