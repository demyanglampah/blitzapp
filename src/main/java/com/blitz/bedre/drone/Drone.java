package com.blitz.bedre.drone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blitz.bedre.model.location.DronePosition;
import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.model.message.MessageHandler;
import com.blitz.bedre.model.message.MsgType;
import com.blitz.bedre.model.message.RequestDronePositionsMessage;
import com.blitz.bedre.model.message.TrafficReportMessage;
import com.blitz.bedre.service.DroneMessagingService;

public class Drone implements Runnable, MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(Drone.class);

	private static int MAX_LOCATION_COUNT = 10;
	private static int MILLISECONDS_TO_MOVE_TO_NEXT_LOCATION = 2000;

	private DroneMessagingService droneMessagingService;

	private BlockingQueue<DronePosition> nextMovesQueue = new LinkedBlockingQueue<DronePosition>(10);

	private String droneId;
	private DronePosition currentLocation;

	private List<DronePosition> locationList = new ArrayList<DronePosition>(MAX_LOCATION_COUNT);

	private List<MessageHandler> messagesHandlers = new ArrayList<MessageHandler>();

	private volatile boolean running = false;

	public Drone(String droneId, DroneMessagingService droneMessagingService) {
		this.droneId = droneId;
		this.droneMessagingService = droneMessagingService;
		droneMessagingService.registerConsumers(this);
		running = true;
		new Thread(this, "Drone_" + droneId).start();
	}

	public boolean canStoreMoreLocations() {
		if (locationList.size() < MAX_LOCATION_COUNT) {
			return true;
		}
		return false;
	}

	public DronePosition moveToNextLocation() {

		currentLocation = nextMovesQueue.poll();

		return currentLocation;
	}

	public void run() {
		try {
			while (running) {
				DronePosition dronePosition = moveToNextLocation();

				if (dronePosition != null) {
					logger.info(droneId + " started moving towards the following position: " + dronePosition);
					Thread.sleep(MILLISECONDS_TO_MOVE_TO_NEXT_LOCATION);
					if (dronePosition.isWithinStationRadius()) {
						logger.info(droneId + " is within a tube station radius - preparing to send Traffic Report. ");
						droneMessagingService.handleMessage(generateTrafficReportMessage(dronePosition));
					}
				} else {
					droneMessagingService.handleMessage(generateDronePositionsRequestMessage());
				}

			}

			System.out.println(
					"Drone " + this.droneId + " finished reporting on traffic. Preparing for safe landing... ");
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean processPositionMessage(DronePosition dronePositionMessage) {
		return nextMovesQueue.offer(dronePositionMessage);
	}

	@Override
	public boolean processShutdownMessage(Message message) {
		if (MsgType.DRONE_SHUTDOWN.equals(message.getType())) {
			running = false;
		}
		return true;
	}

	@Override
	public String getConsumerKey() {
		return droneId;
	}

	private TrafficReportMessage generateTrafficReportMessage(DronePosition dronePosition) {
		return new TrafficReportMessage(droneId, dronePosition.getTime());
	}

	private RequestDronePositionsMessage generateDronePositionsRequestMessage() {
		return new RequestDronePositionsMessage(droneId, nextMovesQueue.remainingCapacity());
	}
}
