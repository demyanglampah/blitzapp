package com.blitz.bedre.drone;

import com.blitz.bedre.model.location.DronePosition;
import com.blitz.bedre.model.message.Message;

public interface MessageListener {
	
	boolean processPositionMessage(DronePosition dronePositionMessage);
	
	boolean processShutdownMessage(Message message);
	
	String getConsumerKey();
}
