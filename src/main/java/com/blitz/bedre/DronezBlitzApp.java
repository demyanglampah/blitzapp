package com.blitz.bedre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DronezBlitzApp {
	
	public static void main(String[] args) {
		SpringApplication.run(DronezBlitzApp.class, args);
	}
}
