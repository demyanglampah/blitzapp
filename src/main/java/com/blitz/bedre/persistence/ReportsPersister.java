package com.blitz.bedre.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.model.message.MessageHandler;

public abstract class ReportsPersister implements MessageHandler{
	
	private static final Logger logger = LoggerFactory.getLogger(ReportsPersister.class);
	
	public void save(Message msg) {
		if (!isPersistable(msg)){
			logger.error("Cannot persist message: " + msg.toString());
		}
		persist(msg);
	}
	
	public abstract boolean isPersistable(Message msg);
	
	public abstract boolean persist(Message msg);
}
