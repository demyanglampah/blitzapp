package com.blitz.bedre.persistence;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.model.message.MsgType;
import com.blitz.bedre.model.message.TrafficReportMessage;
import com.blitz.bedre.service.AsyncTrafficManager;

@Component
public class TrafficReportsPersister extends ReportsPersister {

	private static final Logger logger = LoggerFactory.getLogger(TrafficReportsPersister.class);

	private BlockingQueue<TrafficReportMessage> trafficReportMessagesQueue = new LinkedBlockingQueue<TrafficReportMessage>();

	private ExecutorService taskExecutor = Executors.newSingleThreadExecutor();

	@Override
	public boolean isPersistable(Message msg) {
		if (msg instanceof Serializable) {
			return true;
		}
		return false;
	}

	@Override
	public boolean persist(Message msg) {

		FileOutputStream fo;
		try {
			fo = new FileOutputStream(
					msg.getSource() + "_to_" + msg.getDestination() + "_" + msg.getType() + System.currentTimeMillis());
			ObjectOutputStream so = new ObjectOutputStream(fo);
			so.writeObject(msg);
			so.flush();
			so.close();
			return true;
		} catch (IOException e) {
			logger.debug("Something went wrong with persisting the message: " + msg.getSource() + ":"
					+ msg.getDestination() + ":" + msg.getType());
			e.printStackTrace();
		}

		return false;
	}

	public boolean canHandle(Message message) {
		return getSupportedMessageType().equals(message.getType());
	}

	public MsgType getSupportedMessageType() {
		return MsgType.TRAFFIC_REPORT;
	}

	@Override
	public void execute(Message message) {

		this.taskExecutor.execute(new Runnable() {

			@Override
			public void run() {
				try {
					persist(message);
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}
			}
		});
	}
}
