package com.blitz.bedre;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.blitz.bedre.csv.CsvFileImporter;
import com.blitz.bedre.drone.Drone;
import com.blitz.bedre.service.DroneMessagingService;
import com.blitz.bedre.service.MessagesBroker;

@Component
// @ComponentScan(basePackages = { "com.blitz.*" })
// @PropertySource("classpath:application.properties")
public class BlitzCommandLine implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(BlitzCommandLine.class);

	private static final String DEFAULT_START_SIMULATION_ARGUMENT = "start";

	private boolean canStartSimulation = false;

	@Autowired
	private Environment env;
	@Autowired
	CsvFileImporter csvFileImporter;
	@Autowired
	DroneMessagingService droneMessagingService;
	

	@Override
	public void run(String... args) throws Exception {
		logger.info("Application Arguments: " + args.length);
		for (String argument : args) {
			logger.info(argument);
			if (DEFAULT_START_SIMULATION_ARGUMENT.equalsIgnoreCase(argument)) {
				canStartSimulation = true;
			}
			;
		} 

		if (canStartSimulation) {
			startSimulation();
		}

	}

	private void startSimulation() {
		try {
			logger.info("Starting simulation ... preparing Traffic Drones for vertical take off.");
			
			Drone drone6043 = new Drone(DroneMessagingService.DRONE_6043_ID, droneMessagingService);
			Drone drone5937 = new Drone(DroneMessagingService.DRONE_5937_ID, droneMessagingService);

			MessagesBroker broker = new MessagesBroker();
			
//			CsvFileImporter.readStationsPositionsCsvFile(TUBE_STATIONS_CSV_FILE_PATH, TUBE_STATIONS_CSV_FILE_SKIP_HEADER);
			//CsvFileImporter.readDronePositionsCsvFile(DRONE_POSITIONS_CSV_FILE_PATH, DRONE_POSITIONS_CSV_FILE_SKIP_HEADER);
//			csvFileImporter.readDronePositionsCsvFileAndEnrichLocations(DRONE_POSITIONS_CSV_FILE_PATH, DRONE_POSITIONS_CSV_FILE_SKIP_HEADER);

//			ExecutorService threadPool = Executors.newFixedThreadPool(3);

//			threadPool.execute(new Drone("6043", broker));
//			threadPool.execute(new Drone("5937", broker));
//			Future producerStatus = threadPool.submit(new Producer(broker));

			// this will wait for the producer to finish its execution.
//			producerStatus.get();

//			threadPool.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void printStairCase(int stairCaseHeight){
		
		for(int i=0; i<stairCaseHeight; i++){
			System.out.println();
		}
		
	}
	
	private String drawSeveralCharacters(int blankSpaces, char characterToDraw){
		StringBuilder sb = new StringBuilder();
		for(int i=0 ; i<blankSpaces; i++){
			sb.append(characterToDraw);
		}
		return sb.toString();
	}
}
