package com.blitz.bedre.model.location;

import java.time.LocalTime;

public class DronePosition {
	
	private String droneId;
	private LatLon latLon;
	private LocalTime time;
	
	private boolean withinStationRadius = false;
	
	public DronePosition(String droneId, LatLon latLon, LocalTime time, boolean withinStationRadius) {
		this.droneId = droneId;
		this.latLon = latLon;
		this.time = time;
		this.withinStationRadius = withinStationRadius;
	}


	public boolean isWithinStationRadius() {
		return withinStationRadius;
	}

	public void setWithinStationRadius(boolean withinStationRadius) {
		this.withinStationRadius = withinStationRadius;
	}
				
	public String getDroneId() {
		return droneId;
	}
	public void setDroneId(String droneId) {
		this.droneId = droneId;
	}
	public LatLon getLatLon() {
		return latLon;
	}
	public void setLatLon(LatLon latLon) {
		this.latLon = latLon;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "DronePosition [droneId=" + droneId + ", latLon=" + latLon + ", time=" + time + ", withinStationRadius="
				+ withinStationRadius + "]";
	}
}
