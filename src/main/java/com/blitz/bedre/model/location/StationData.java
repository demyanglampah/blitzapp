package com.blitz.bedre.model.location;

public class StationData {
	private String stationName;
	private LatLon latLon;
	
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public LatLon getLatLon() {
		return latLon;
	}
	public void setLatLon(LatLon latLon) {
		this.latLon = latLon;
	}
}
