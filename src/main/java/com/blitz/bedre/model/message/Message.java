package com.blitz.bedre.model.message;

public interface Message {

	String getSource();

	String getDestination();

	MsgType getType();

	String getPayload();
}
