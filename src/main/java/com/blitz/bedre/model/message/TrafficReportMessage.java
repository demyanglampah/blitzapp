package com.blitz.bedre.model.message;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Random;

import com.blitz.bedre.service.AsyncTrafficManager;

//o Drone ID o Time o Speed o Conditions of Traffic (HEAVY, LIGHT, MODERATE). This can be chosen randomly.
public class TrafficReportMessage implements Message, Serializable {
	
	private String source;
	private TrafficCondition trafficCondition;
	private LocalTime time;
	private int trafficSpeed;

	public TrafficReportMessage(String source, LocalTime time) {
		this.source = source;
		this.time = time;
		this.trafficSpeed = new Random().nextInt(100);
		this.trafficCondition = (TrafficCondition.values())[new Random().nextInt(3)];
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public String getDestination() {
		return AsyncTrafficManager.class.getSimpleName();
	}

	@Override
	public MsgType getType() {
		return MsgType.TRAFFIC_REPORT;
	}

	@Override
	public String getPayload() {
		StringBuilder sb = new StringBuilder("TrafficReport as reported by: ");
		sb.append("\n");
		sb.append("Drone ID: " + source);
		sb.append("\n");
		sb.append("Time: " + time);
		sb.append("\n");
		sb.append("Speed: " + trafficSpeed);
		sb.append("\n");
		sb.append("Traffic Condition: " + trafficCondition);
		return sb.toString();
	}

}
