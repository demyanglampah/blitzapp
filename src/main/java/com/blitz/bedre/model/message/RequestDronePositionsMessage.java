package com.blitz.bedre.model.message;

import com.blitz.bedre.service.AsyncTrafficManager;

public class RequestDronePositionsMessage implements Message {
	
	private String source;
	private int minAvailableDronePositionsCapacity;

	public RequestDronePositionsMessage(String source, int minAvailableDronePositionsCapacity) {
		this.source = source;
		this.minAvailableDronePositionsCapacity = minAvailableDronePositionsCapacity;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public String getDestination() {
		return AsyncTrafficManager.class.getSimpleName();
	}

	@Override
	public MsgType getType() {
		return MsgType.REQUEST_LOCATION;
	}

	@Override
	public String getPayload() {
		return minAvailableDronePositionsCapacity + "";
	}

}
