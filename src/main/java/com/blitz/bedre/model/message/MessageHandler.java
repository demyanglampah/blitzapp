package com.blitz.bedre.model.message;

public interface MessageHandler {
	
	MsgType getSupportedMessageType();
	
	boolean canHandle(Message message);
	
	void execute(Message message);

}
