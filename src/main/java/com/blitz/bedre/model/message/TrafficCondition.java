package com.blitz.bedre.model.message;

public enum TrafficCondition {
	HEAVY,
	
	MODERATE,
	
	LIGHT

}
