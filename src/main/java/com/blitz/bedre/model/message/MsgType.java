package com.blitz.bedre.model.message;

public enum MsgType {
	
	DRONE_SHUTDOWN,
	
	DRONE_MOVE,
	
	TRAFFIC_REPORT,
	
	REQUEST_LOCATION

}
