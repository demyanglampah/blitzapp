package com.blitz.bedre.model.message;

public class DroneShutDownMessage implements Message {
	
	private String source;
	private String destination;
	private MsgType type;

	public DroneShutDownMessage(String source, String destination) {
		this.type = MsgType.DRONE_SHUTDOWN;
		this.source = source;
		this.destination = destination;
	}

	@Override
	public String getSource() {
		// TODO Auto-generated method stub
		return source;
	}

	@Override
	public String getDestination() {
		// TODO Auto-generated method stub
		return destination;
	}

	@Override
	public MsgType getType() {
		return type;
	}

	@Override
	public String getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

}
