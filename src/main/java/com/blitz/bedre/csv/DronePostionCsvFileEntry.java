package com.blitz.bedre.csv;

import java.time.LocalTime;

public class DronePostionCsvFileEntry {

	// The csv file layout is drone id,latitude,longitude,time

	public static final int DRONE_ID = 0;
	public static final int LAT = 1;
	public static final int LON = 2;
	public static final int TIME = 3;

	public DronePostionCsvFileEntry(String droneId, Double lat, Double lon, LocalTime time) {
		super();
		this.droneId = droneId;
		this.lat = lat;
		this.lon = lon;
		this.time = time;
	}

	private String droneId;
	private Double lat;
	private Double lon;
	private LocalTime time;

	public String getDroneId() {
		return droneId;
	}

	public void setDroneId(String droneId) {
		this.droneId = droneId;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "DronePostionCsvFileEntry [droneId=" + droneId + ", lat=" + lat + ", lon=" + lon + ", time=" + time
				+ "]";
	}
}
