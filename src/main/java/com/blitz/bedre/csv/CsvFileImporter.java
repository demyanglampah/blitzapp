package com.blitz.bedre.csv;

import static com.blitz.bedre.csv.DronePostionCsvFileEntry.DRONE_ID;
import static com.blitz.bedre.csv.DronePostionCsvFileEntry.LAT;
import static com.blitz.bedre.csv.DronePostionCsvFileEntry.LON;
import static com.blitz.bedre.csv.DronePostionCsvFileEntry.TIME;
import static com.blitz.bedre.csv.StationPositionCsvFileEntry.LAT_INDEX;
import static com.blitz.bedre.csv.StationPositionCsvFileEntry.LON_INDEX;
import static com.blitz.bedre.csv.StationPositionCsvFileEntry.STATION_NAME_INDEX;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.blitz.bedre.model.location.DronePosition;
import com.blitz.bedre.model.location.LatLon;
import com.blitz.bedre.model.message.Message;
import com.blitz.bedre.util.GeoLocation;

@Component
@Configuration
@ComponentScan(basePackages = { "com.blitz.*" })
@PropertySource("classpath:application.properties")
public class CsvFileImporter {

	private static final Logger logger = LoggerFactory.getLogger(CsvFileImporter.class);

	private static final String COMMA_CSV_FILE_DELIMITER = ",";
	
	private static String DRONE_POSITIONS_CSV_FILE_PATH;
	private static boolean DRONE_POSITIONS_CSV_FILE_SKIP_HEADER;
	private static String TUBE_STATIONS_CSV_FILE_PATH;
	private static boolean TUBE_STATIONS_CSV_FILE_SKIP_HEADER;
	private static float RADIUS_FOR_TRAFFIC_REPORT;

	private static final String DRONE_6043_ID = "6043";
	private static final String DRONE_5937_ID = "5937";
	
	private static final double EARTH_RADIUS_IN_METERS = 6371.01 * 1000;
	
	@Autowired 
	private Environment env;

	private BlockingQueue<DronePosition> drone6043PositionsQueue = new LinkedBlockingQueue<DronePosition>();
	private BlockingQueue<DronePosition> drone5937PositionsQueue = new LinkedBlockingQueue<DronePosition>();

	private List<StationPositionCsvFileEntry> tubeStationsCsvFileEntries = new ArrayList<StationPositionCsvFileEntry>();

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	public void init(){
		DRONE_POSITIONS_CSV_FILE_SKIP_HEADER = Boolean.valueOf(env.getProperty("drone.positions.file.skip.header"));
		DRONE_POSITIONS_CSV_FILE_PATH = env.getProperty("drone.positions.file.location");
		
		TUBE_STATIONS_CSV_FILE_PATH = env.getProperty("tube.stations.positions.file.location");
		TUBE_STATIONS_CSV_FILE_SKIP_HEADER = Boolean.valueOf(env.getProperty("tube.stations.positions.file.skip.header"));
		
		RADIUS_FOR_TRAFFIC_REPORT = Float.valueOf(env.getProperty("traffic.report.radius"));
		
		readDronePositionsCsvFileAndEnrichLocations(DRONE_POSITIONS_CSV_FILE_PATH, DRONE_POSITIONS_CSV_FILE_SKIP_HEADER);
	}
	
	public DronePosition getNextPositionFromQueue(Message message) {
		if (DRONE_6043_ID.equals(message.getSource()))
			return drone6043PositionsQueue.poll();
		else if (DRONE_5937_ID.equals(message.getSource()))
			return drone5937PositionsQueue.poll();
		else
			logger.error("There is no Drone registered with ID: " + message.getSource());
		return null;
	}

	private void readDronePositionsCsvFileAndEnrichLocations(String filePath, boolean skipHeader) {

		try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath));) {
			
			String line = "";

			// Read the CSV file header in order to skip it if needed
			if (skipHeader) {
				fileReader.readLine();
			}

			StopWatch stopWatch = new StopWatch("CSV Drone Positions and Tube Stations information processing ...");

			stopWatch.start("Start loading Tube Stations GeoPoint Positions from the CSV file ... " + TUBE_STATIONS_CSV_FILE_PATH);
			
//			tubeStationsCsvFilePath = env.getProperty("tube.stations.positions.file.location");
//			skipHeaderInTubeStationsFile = Boolean.valueOf(env.getProperty("tube.stations.positions.file.skip.header"));

			tubeStationsCsvFileEntries = readStationsPositionsCsvFile(TUBE_STATIONS_CSV_FILE_PATH, TUBE_STATIONS_CSV_FILE_SKIP_HEADER);

			stopWatch.stop();
			
			logger.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis() + " ms to execute!");

			stopWatch.start("Start loading Drone Positions and enrich with Tube Stations information");

			while ((line = fileReader.readLine()) != null) {

				String[] tokens = line.split(COMMA_CSV_FILE_DELIMITER);
				if (tokens.length > 0) {
					DronePosition dronePosition = new DronePosition(tokens[DRONE_ID],
							new LatLon(Double.valueOf(tokens[LAT]), Double.valueOf(tokens[LON])),
							LocalTime.parse(tokens[TIME]), false);
					
					enrichDronePositionWithTubeStationInformation(dronePosition);

					logger.info(dronePosition.toString());

					if (DRONE_6043_ID.equals(dronePosition.getDroneId())) {
						drone6043PositionsQueue.add(dronePosition);
					} else if (DRONE_5937_ID.equals(dronePosition.getDroneId())) {
						drone5937PositionsQueue.add(dronePosition);
					} else {
						logger.warn("Discarding drone position with unknown ID: " + dronePosition.toString());
					}
				}
			}
			stopWatch.stop();
			
			logger.info(stopWatch.getLastTaskName() + " took " + stopWatch.getLastTaskTimeMillis() + " ms to execute!");
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		}

	}
	
	private void enrichDronePositionWithTubeStationInformation(DronePosition dronePosition){
		for(StationPositionCsvFileEntry stationLocation : tubeStationsCsvFileEntries){
			if (isLocationWithinRadius(dronePosition.getLatLon(), new LatLon(stationLocation.getLat(), stationLocation.getLon()))){
				dronePosition.setWithinStationRadius(true);
				logger.info(dronePosition + " is within a 350m radius of tube station: " + stationLocation);
				break;
			}
		}
	}
	
	private boolean isLocationWithinRadius(LatLon droneLocation, LatLon stationLocation){
		GeoLocation geoLocation = GeoLocation.fromDegrees(droneLocation.getLat(), droneLocation.getLon());
		double distanceToPoint = geoLocation.distanceTo(GeoLocation.fromDegrees(stationLocation.getLat(), stationLocation.getLon()), EARTH_RADIUS_IN_METERS);
		if (RADIUS_FOR_TRAFFIC_REPORT - distanceToPoint >= 0){
			return true;
		} 
		return false;
	}

	private static void readStationsPositionsFromCsvFileAndStoreThem(String filePath, boolean skipHeader) {
		try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath));) {

			// Create a new list of csvFileEntry to be filled by CSV file data
			List<StationPositionCsvFileEntry> csvFileEntrys = new ArrayList<StationPositionCsvFileEntry>();

			String line = "";

			File stationsFile = new File(filePath);

			if (!stationsFile.exists()) {
				logger.error("Cannot find file: " + stationsFile.getAbsolutePath());
			}

			// Skip the CSV file header
			if (skipHeader) {
				fileReader.readLine();
			}

			while ((line = fileReader.readLine()) != null) {

				String[] tokens = line.split(COMMA_CSV_FILE_DELIMITER);
				if (tokens.length > 0) {

					StationPositionCsvFileEntry csvFileEntry = new StationPositionCsvFileEntry(
							tokens[STATION_NAME_INDEX], Double.valueOf(tokens[LAT_INDEX]),
							Double.valueOf(tokens[LON_INDEX]));
					csvFileEntrys.add(csvFileEntry);

					// fileWriter.write(csvFileEntry.getStationName() +
					// COMMA_CSV_FILE_DELIMITER + csvFileEntry.getLat()
					// + COMMA_CSV_FILE_DELIMITER + csvFileEntry.getLon() +
					// "\n");

				}
			}

			// Print the new csvFileEntry list
			for (StationPositionCsvFileEntry csvFileEntry : csvFileEntrys) {
				System.out.println(csvFileEntry.toString());
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		}
	}

	public static void readDronePositionsCsvFile(String filePath, boolean skipHeader) {

		try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(filePath + "_edited"));) {

			// Create a new list of csvFileEntry to be filled by CSV file data
			List<DronePostionCsvFileEntry> csvFileEntries = new ArrayList<DronePostionCsvFileEntry>();

			String line = "";

			// Read the CSV file header to skip it
			if (skipHeader) {
				fileReader.readLine();
			}

			int count = 0;

			LocalTime time = LocalTime.of(6, 0);

			while ((line = fileReader.readLine()) != null) {
				// Get all tokens available in line

				count++;

				String[] tokens = line.split(COMMA_CSV_FILE_DELIMITER);
				if (tokens.length > 0) {
					// Create a new csvFileEntry object and fill his data
					DronePostionCsvFileEntry csvFileEntry = new DronePostionCsvFileEntry(tokens[DRONE_ID],
							Double.valueOf(tokens[LAT]), Double.valueOf(tokens[LON]), LocalTime.parse(tokens[TIME]));

					// DronePostionCsvFileEntry csvFileEntry = new
					// DronePostionCsvFileEntry("ASAx",
					// Double.valueOf(tokens[LAT]), Double.valueOf(tokens[LON]),
					// time);

					csvFileEntries.add(csvFileEntry);

					StringBuilder sb = new StringBuilder();

					if (count % 2 == 0) {
						sb.append("6043");
					} else {
						sb.append("5937");
					}

					sb.append(COMMA_CSV_FILE_DELIMITER);
					sb.append(csvFileEntry.getLat());
					sb.append(COMMA_CSV_FILE_DELIMITER);
					sb.append(csvFileEntry.getLon());
					sb.append(COMMA_CSV_FILE_DELIMITER);
					sb.append(time);

					// fileWriter.write(sb.toString() + "\n");
					if (count % 2 == 0) {
						time = time.plusMinutes(2);
					}
				}
			}

			// Print the new csvFileEntry list
			for (DronePostionCsvFileEntry csvFileEntry : csvFileEntries) {
				System.out.println(csvFileEntry.toString());
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		}

	}

	public List<StationPositionCsvFileEntry> readStationsPositionsCsvFile(String filePath, boolean skipHeader) {
		// Create a new list of csvFileEntry to be filled by CSV file data
		List<StationPositionCsvFileEntry> csvFileEntries = new ArrayList<StationPositionCsvFileEntry>();

		try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(filePath + "_edited"));) {

			String line = "";

			File stationsFile = new File(filePath);

			if (!stationsFile.exists()) {
				logger.error("Cannot find file: " + stationsFile.getAbsolutePath());
			}

			// Skip the CSV file header
			if (skipHeader) {
				fileReader.readLine();
			}

			while ((line = fileReader.readLine()) != null) {

				String[] tokens = line.split(COMMA_CSV_FILE_DELIMITER);
				if (tokens.length > 0) {

					StationPositionCsvFileEntry csvFileEntry = new StationPositionCsvFileEntry(
							tokens[STATION_NAME_INDEX], Double.valueOf(tokens[LAT_INDEX]),
							Double.valueOf(tokens[LON_INDEX]));
					csvFileEntries.add(csvFileEntry);

					// fileWriter.write(csvFileEntry.getStationName() +
					// COMMA_CSV_FILE_DELIMITER + csvFileEntry.getLat()
					// + COMMA_CSV_FILE_DELIMITER + csvFileEntry.getLon() +
					// "\n");

				}
			}

			// Print the new csvFileEntry list
			for (StationPositionCsvFileEntry csvFileEntry : csvFileEntries) {
				System.out.println(csvFileEntry.toString());
			}

		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		}

		return csvFileEntries;
	}

}
