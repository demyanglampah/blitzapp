package com.blitz.bedre.csv;

public class StationPositionCsvFileEntry {

	// There is also a file with the lat/lon points for London tube stations. station,lat,lon
	private String stationName;
	private Double lat;
	private Double lon;

	public static final int STATION_NAME_INDEX = 0;
	public static final int LAT_INDEX = 1;
	public static final int LON_INDEX = 2;

	public StationPositionCsvFileEntry(String stationName, Double lat, Double lon) {
		super();
		this.stationName = stationName;
		this.lat = lat;
		this.lon = lon;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "StationPositionCsvFileEntry [stationName=" + stationName + ", lat=" + lat + ", lon=" + lon + "]";
	}
}
